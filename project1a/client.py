import sys
import random
import struct
from socket import socket, SOCK_DGRAM, AF_INET

# Hard code the address and port
SERVER_ADDRESS = ('kronos.escher5d.com', 7775)
# Define constants
GAME_ID_SIZE = 24
MESSAGE_ID_SIZE = 8
GAME_FLAGS_SIZE = 14
GAME_STATE_SIZE = 18

# Sets up the board as a 3x3 array
def setup_board():
    return [' '] * 9

# Winning combinations set as completion of rows, columns and diagonals
def check_winner(board):
    winning_combinations = [
        (0, 1, 2), (3, 4, 5), (6, 7, 8),
        (0, 3, 6), (1, 4, 7), (2, 5, 8),
        (0, 4, 8), (2, 4, 6)
    ]
    for x, y, z in winning_combinations:
        if board[x] == board[y] == board[z] and board[x] != ' ':
            return board[x]
    return None

# Function to check if a move is valid based on the position selected and whether or not it's empty
def valid_move(board, move):
    return 0 <= move < 9 and board[move] == ' '

# A visual display of the board
def display_board(board):
    for i in range(0, 9, 3):
        print("|".join(board[i:i+3]))
    print("\n")

# Message encoder
def encode_msg(game_id, message_id, flags, board, message):
    game_state = encode_game_state(board)
    header = (game_id << (MESSAGE_ID_SIZE + GAME_FLAGS_SIZE + GAME_STATE_SIZE)) | \
             (message_id << (GAME_FLAGS_SIZE + GAME_STATE_SIZE)) | \
             (flags << GAME_STATE_SIZE) | game_state
    packed_header = struct.pack("!Q", header)
    return packed_header + message.encode('utf-8')

# Message decoder
def decode_msg(data):
    header, = struct.unpack("!Q", data[:8])
    text = data[8:].decode('utf-8')

    game_id = (header >> (MESSAGE_ID_SIZE + GAME_FLAGS_SIZE + GAME_STATE_SIZE)) & ((1 << GAME_ID_SIZE) - 1)
    message_id = (header >> (GAME_FLAGS_SIZE + GAME_STATE_SIZE)) & ((1 << MESSAGE_ID_SIZE) - 1)
    flags = (header >> GAME_STATE_SIZE) & ((1 << GAME_FLAGS_SIZE) - 1)
    game_state = header & ((1 << GAME_STATE_SIZE) - 1)

    board = decode_game_state(game_state)
    return game_id, message_id, flags, board, text

# Returns the game ID
def get_game_id():
    return random.getrandbits(GAME_ID_SIZE)

# Returns the message ID
def get_message_id():
    return random.getrandbits(MESSAGE_ID_SIZE)


def encode_game_state(board):
    state = 0
    for i, cell in enumerate(board):
        if cell == 'X':
            state |= (1 << (i * 2 + 1))
        elif cell == 'O':
            state |= (1 << (i * 2))
    return state

def decode_game_state(state):
    board = [' '] * 9
    for i in range(9):
        # Shift binary represenation of the state right and mask all the bits except the rightmost two
        cell_state = (state >> (i * 2)) & 0b11
        if cell_state == 0b01:
            board[i] = 'X'
        elif cell_state == 0b10:
            board[i] = 'O'
    return board

# Takes in the player's move and ensure the input is an integer 
def get_player_move():
    move = input("Enter move (0-8): ")
    while not move.isdigit() or int(move) not in range(9):
        move = input("Enter move (0-8): ")
    return int(move)

def main():
    with socket(AF_INET, SOCK_DGRAM) as client_socket:
        # Set timeout to 5 seconds if no response from server for debugging
        client_socket.settimeout(5)
        player_name = input("Enter name: ")
        
        # Initialize the game
        game_id = get_game_id()
        msg_id = get_message_id()
        board = setup_board()
        
        # Send initial message to the server to start the game
        initial_message = encode_msg(game_id, msg_id, 0, board, player_name)
        client_socket.sendto(initial_message, SERVER_ADDRESS)
        
        # Loop the game
        while True:
            display_board(board)
            # Player's move
            move = get_player_move()
            while not valid_move(board, move):
                print("Invalid move.")
                move = get_player_move()
            board[move] = 'X'
            
            # Send move to server
            msg_id = get_message_id()
            client_socket.sendto(encode_msg(game_id, msg_id, 0, board, "Player's move"), SERVER_ADDRESS)

            # Receive server's move
            try:
                data, addr = client_socket.recvfrom(1024)
                _, _, _, received_board, text = decode_msg(data)
                board = received_board
            except socket.timeout:
                print("No response from server. Exiting...")
                sys.exit()

            # Check for winner
            winner = check_winner(board)
            if winner:
                display_board(board)
                if winner == 'X':
                    print(f"{player_name} wins!")
                else:
                    print("Server wins!")
                break

if __name__ == "__main__":
    main()
