import socket
import random
import struct
import sys
import threading

# Define constants similar to the client side
SERVER_ADDRESS = ('localhost', 7775)  # You can choose another port if needed
GAME_ID_SIZE = 24
MESSAGE_ID_SIZE = 8
GAME_FLAGS_SIZE = 14
GAME_STATE_SIZE = 18

############################
# Game functions
############################
# Set up the board as a 3x3 array
def setup_board():
    return [' '] * 9

# Winning combinations set as completion of rows, columns and diagonals
def check_winner(board):
    winning_combinations = [
        (0, 1, 2), (3, 4, 5), (6, 7, 8),
        (0, 3, 6), (1, 4, 7), (2, 5, 8),
        (0, 4, 8), (2, 4, 6)
    ]
    for x, y, z in winning_combinations:
        if board[x] == board[y] == board[z] and board[x] != ' ':
            return board[x]
    return None

# Function to check if a move is valid based on the position selected and whether or not it's empty
def valid_move(board, move):
    return 0 <= move < 9 and board[move] == ' '

# A visual display of the board
def display_board(board):
    for i in range(0, 9, 3):
        print("|".join(board[i:i+3]))
    print("\n")

# Message encoder
def encode_msg(game_id, message_id, flags, board, message):
    game_state = encode_game_state(board)
    header = (game_id << (MESSAGE_ID_SIZE + GAME_FLAGS_SIZE + GAME_STATE_SIZE)) | \
            (message_id << (GAME_FLAGS_SIZE + GAME_STATE_SIZE)) | \
            (flags << GAME_STATE_SIZE) | game_state
    packed_header = struct.pack("!Q", header)
    return packed_header + message.encode('utf-8')

# Message decoder
def decode_msg(data):
    header, = struct.unpack("!Q", data[:8])
    text = data[8:].decode('utf-8')

    game_id = (header >> (MESSAGE_ID_SIZE + GAME_FLAGS_SIZE + GAME_STATE_SIZE)) & ((1 << GAME_ID_SIZE) - 1)
    message_id = (header >> (GAME_FLAGS_SIZE + GAME_STATE_SIZE)) & ((1 << MESSAGE_ID_SIZE) - 1)
    flags = (header >> GAME_STATE_SIZE) & ((1 << GAME_FLAGS_SIZE) - 1)
    game_state = header & ((1 << GAME_STATE_SIZE) - 1)

    board = decode_game_state(game_state)
    return game_id, message_id, flags, board, text

# Returns the game ID
def get_game_id():
    return random.getrandbits(GAME_ID_SIZE)

# Returns the message ID
def get_message_id():
    return random.getrandbits(MESSAGE_ID_SIZE)


def encode_game_state(board):
    state = 0
    for i, cell in enumerate(board):
        if cell == 'X':
            state |= (1 << (i * 2 + 1))
        elif cell == 'O':
            state |= (1 << (i * 2))
    return state

def decode_game_state(state):
    board = [' '] * 9
    for i in range(9):
        # Shift binary represenation of the state right and mask all the bits except the rightmost two
        cell_state = (state >> (i * 2)) & 0b11
        if cell_state == 0b01:
            board[i] = 'X'
        elif cell_state == 0b10:
            board[i] = 'O'
    return board

############################
# /Game functions
############################

# Function to make a move by the server
def server_move(board):
    # just choose the first available position
    for i, cell in enumerate(board):
        if cell == ' ':
            board[i] = 'X'
            return
        
def client_threads(client_data, client_address, server_socket):
    data = client_data
    while data:
        # Decode the client's message
        game_id, message_id, flags, board, text = decode_msg(data)
        print(f"Received message from {client_address}: {text}")
        print("Current board state:")
        display_board(board)

        winner = check_winner(board)
        if winner:
            print(f"Game over: {winner} wins!")
            break  # if a client wins, close the thread

        server_move(board)
        print("Board after server's move:")
        display_board(board)

        winner = check_winner(board)
        if winner:
            print(f"{winner} wins!")
            break
        # Here, the client side will get this message to acknowledge that the server made a move
        response_message = "Server made a move."
        encoded_response = encode_msg(game_id, get_message_id(), flags, board, response_message)

        # respond to client inputs
        server_socket.sendto(encoded_response, client_address)

        # listen for client inputs
        data, _ = server_socket.recvfrom(1024)

    print(f"Closing connection with {client_address}.")

def main():
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    server_socket.bind(SERVER_ADDRESS)
    
    print(f"Server address: {SERVER_ADDRESS}")

    while True:
        try:
            # listen for client inputs
            data, client_address = server_socket.recvfrom(1024)
            # handle multiple clients with threading
            threading.Thread(target=client_threads, args=(data, client_address, server_socket)).start()
            
        except KeyboardInterrupt:
            print("Server shutting down.")  # shut down server
            break
        except Exception as e: 
            print(f"An unexpected error occurred: {e}")
    server_socket.close() 

if __name__ == "__main__":
    main()