# TCP Server Client

## Overview

This README provides instructions on how to build, run, and use the TCP chat server. The server allows clients to connect to it, join chat rooms and send messages to specific chat rooms or users.

## Prerequisites

-Python 3.7 or higher
-Linux

## How to run

To run the TCP server client, use the command.

```
python3 server.py <IP Address> <Desired server port>
```


Example:
```
python3 server.py 127.0.0.1 7775
```