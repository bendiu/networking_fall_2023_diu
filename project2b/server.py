import sys
import json
from socket import socket, AF_INET, SOCK_STREAM
from select import select

# Global variables to store the JSON payloads for messaging
clients = {}  # example: {client_socket: 'user_name'}
subscriptions = {}  # stores user-chatroom assignments - {'user_name': ['channel1', 'channel2']}
message_queues = {}  # stores messages for each user - {'user_name': [{'target': '...', 'from': '...', 'message': '...'}]}

# Used to send queued messages to a client
def send_messages(sock):
    user_name = clients[sock]
    queue = message_queues.get(user_name, [])
    while queue:
        message = queue.pop(0)
        try:
            sock.send(json.dumps(message).encode('utf-8'))  # Send the JSON payloads
        except:
            # Handle exceptions
            pass

# Function to queue error messages to be sent to a client
def send_error(sock, error_message):
    error_msg = json.dumps({"status": "error", "message": error_message})
    message_queues[clients[sock]].append(error_msg)

# Receive and process JSON payloads and handle errors from the server level
def recv_data(sock):
    try:
        data = sock.recv(4096)  # Set the message size limit (4096 bytes).
        if not data:
            return False  # No data, then disconnected
        try:
            # Attempt to decode and load the JSON message
            message = json.loads(data.decode('utf-8'))  # Messages must be valid JSON and UTF-8.
            print(f"Received message from {sock}: {message}")  # For printing the messages
            handle_client_message(sock, message)
        except json.JSONDecodeError:
            # Handle malformed JSON.
            send_error(sock, "Malformed JSON")
        except UnicodeDecodeError:
            # Handle malformed UTF-8 data.
            send_error(sock, "Malformed UTF-8 Data")
        return True
    except Exception as e:
        # General exception handling
        print(f"Error in recv_data: {e}")
        return False

# Function to handle client messages
def handle_client_message(sock, message):
    try:
        # Check the message size to ensure it doesn't exceed the limit.
        if len(json.dumps(message)) > 4096:
            send_error(sock, "Message Longer Than 4096 Bytes")
            return

        action = message.get("action")
        if action not in ["connect", "message", "disconnect"]:
            # Handle invalid action
            send_error(sock, "Invalid action")
            return

        if action == "connect":
            process_connect(sock, message)  # Handle client connections.
        elif action == "message":
            process_message(sock, message)  #  Handle sending/receiving messages.
        elif action == "disconnect":
            # Handle disconnections
            pass
    except Exception as e:
        print(f"Error in handle_client_message: {e}")
        send_error(sock, "Server Error")

# Function to process a connection request from a client
def process_connect(sock, message):
    user_name = message.get("user_name")
    targets = message.get("targets")
    if not user_name or not targets:
        # Handle missing required fields
        send_error(sock, "Missing Required Field")
        return
    if len(user_name.encode('utf-8')) > 60 or any(len(t.encode('utf-8')) > 60 for t in targets):
        # Check field length limits
        send_error(sock, "Fields Longer Than Limits")
        return

    clients[sock] = user_name
    subscriptions[user_name] = targets
    message_queues[user_name] = []
    print(f"User {user_name} connected and subscribed to {targets}")  # Print user connection status

# Function to process a message sent from a client
def process_message(sock, message):
    target = message.get("target")
    from_user = message.get("user_name")
    message_content = message.get("message")

    if not target or not from_user or not message_content:
        # Handle missing required fields.
        send_error(sock, "Missing Required Field")
        return
    if any(len(field.encode('utf-8')) > 60 for field in [target, from_user]) or len(message_content.encode('utf-8')) > 3800:
        # Check field length limits
        send_error(sock, "Fields Longer Than Limits")
        return

    print(f"Processing message from {from_user} to {target}: {message_content}")  # Logging

    if target.startswith("#"): # Symbol for chat room
        for user, channels in subscriptions.items():
            if target in channels:
                message_queues[user].append(message)  # Handle chat room messaging
    else:  # Handle direct messaging 
        if target in subscriptions:
            message_queues[target].append(message) 


def main():
    if len(sys.argv) != 3:
        print("Usage: python server.py <IP> <Port>")
        sys.exit(1)

    ip = sys.argv[1]
    port = int(sys.argv[2])

    server_sock = socket(AF_INET, SOCK_STREAM)
    server_sock.bind((ip, port))
    server_sock.listen(10)
    server_sock.setblocking(0)

    read_sockets = [server_sock]
    write_sockets = []

    while True:
        try:
            readlist, writelist, _ = select(read_sockets, write_sockets, [], 2)
            for sock in readlist:
                if sock is server_sock:
                    # Handle new incoming connections
                    client_sock, addr = server_sock.accept()
                    client_sock.setblocking(0)
                    read_sockets.append(client_sock)
                else:
                    # Receive data from existing clients
                    if not recv_data(sock):
                        read_sockets.remove(sock)
                        sock.close()

            for sock in writelist:
                # Send queued messages to clients
                send_messages(sock)

        except KeyboardInterrupt:
            break
        except Exception as e:
            pass

    server_sock.close()

if __name__ == "__main__":
    main()
