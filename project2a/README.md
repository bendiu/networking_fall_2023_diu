# TCP Chat Client

## Overview

This README provides instructions on how to build, run, and use the TCP chat client. The client allows users to connect to a chat server, join chat rooms, send messages to specific chat rooms or users, and receive chat history from the server. See the comments for additional details.

## Prerequisites

-Python 3.x

-Linux

## How to run

To run the TCP chat client, use the command.

```
python3 client.py <IP Address> <Server port>
```

Replace <IP Address> with the IP address of the chat server and <Server port> with the port number.

Example:
```
python3 client.py 127.0.0.1 7775
```

## Usage

Once the client is running and connected to a server, you can interact with the client in a few ways:

### Connecting to Chat Room(s)

Enter your username and the chat rooms you wish to enter.

Example:

```
@Miranda #networking #music #general
```

### Send a Message to a Chat Room

To send a message to users inside a chat room, type the chat room name followed by the message.

```
#networking Hello, networking folks!
```

### Send a Direct Message to a User

To send a direct message to another user, type the user's name followed by the message.

Example:

```
@Prospero Hello Prospero!
```
This sends a private message to the user @Prospero.

### Disconnect from the Server

To disconnect from the server, type 'quit' and press Enter. You can also do a keyboard interrupt by press CTRL + C (might have to do this twice).

### Message Structure

The client will construct messages in JSON format to communicate with the server.

Connect message example:

```
{
    "action": "connect",
    "user_name": "@Miranda",
    "targets": ["#general", "#random"]
}
```

Message to a chat room example:

```
{
    "action": "message",
    "user_name": "@Miranda",
    "target": "#general",
    "message": "Hello, everyone!"
}
```

Direct message to a user example:

```
{
    "action": "message",
    "user_name": "@john",
    "target": "@jane",
    "message": "Hi Jane, how are you?"
}
```

Disconnect message example:

```
{
    "action": "disconnect"
}
```
