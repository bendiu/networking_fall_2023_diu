#!/usr/bin/env python3

import sys
import json
from socket import socket, SOCK_STREAM, AF_INET
from select import select

# Error printing (from tcp_client.py)
def print_error(e, f="UNKNOWN"):
    print("Error in %s!" % (f))
    print(e)
    print(type(e))

# Constructs the JSON action packet for connecting to specific chat rooms.
def construct_connect_message(user, targets):
    return json.dumps({
        "action": "connect", # set action to connect
        "user_name": user,
        "targets": targets
    })

# Constructs the JSON packet for sending a chat message to a user or chat room.
def construct_message(user, target, message):
    return json.dumps({
        "action": "message", # set action to message
        "user_name": user,
        "target": target,
        "message": message
    })

# Constructs a JSON packet for disconnects when the user disconnects from the server.
def construct_disconnect_message():
    return json.dumps({"action": "disconnect"}) # set action to disconnect

# Parses any message received from the server and displays chat message history, disconnect messages or error messages.
def parse_server_message(data):
    try:
        msg = json.loads(data)
        if msg["status"] == "chat":
            # Client Listen: Displays chat message history from the server
            for history_item in msg["history"]:
                print(f"From {history_item['from']} to {history_item['target']}: {history_item['message']}")
        elif msg["status"] == "disconnect":
            # Let the user know that the server is shutting down
            print("Server is shutting down.")
            return 'quit'
        elif 'error' in msg:
            # Error Messages: Display any error messages from the server
            print("Error from server:", msg['error'])
    except Exception as e:
        print_error(e, "Something went wrong with the parse_server_message function")
    return ""

# Sends data to the server (from tcp_client.py)
def send_data(tcp_sock, data):
    try:
        ret = tcp_sock.send(bytes(data, 'utf-8'))
    except KeyboardInterrupt as k:
        raise KeyboardInterrupt()
    except Exception as e:
        print_error(e, "send")

# Receives data from the server (from tcp_client.py)
def recv_data(tcp_sock):
    try:
        data = tcp_sock.recv(4096)
        if len(data) == 0:
            return 'quit'
        return parse_server_message(data.decode('utf-8'))
    except Exception as e:
        print_error(e, "recv")
    return ""

def main():
    if len(sys.argv) >= 3:
        ip = sys.argv[1]
        try:
            port = int(sys.argv[2])
        except:
            print("Port %s unable to be converted to number, run with HOST PORT" % (sys.argv[2]))
            sys.exit(1)

    try:
        tcp_sock = socket(AF_INET, SOCK_STREAM)
        tcp_sock.connect((ip, port))  # Attempt to connect to the server
    except Exception as e:
        print_error(e, "Could not connect to the server")
        sys.exit(1)

    try:
        tcp_sock.setblocking(0)
        read_sockets = [tcp_sock, sys.stdin]
        user_name = None
        subscribed_rooms = []

        while True:
            readlist, _, _ = select(read_sockets, [], [])
            # Process incoming data from the server
            if tcp_sock in readlist:
                data = recv_data(tcp_sock)
                if data == 'quit': # type in "quit" to disconnect
                    # Client Disconnect: Handling for when the server drops out
                    break
            
            # Here is the messaging logic
            if sys.stdin in readlist:
                data = sys.stdin.readline()
                # This block checks for a message starting with @ and if a user name is not already established, then it will create one and connect it with any chat rooms specified
                # @Joe #networking #music #general
                if data.startswith('@') and not user_name: 
                    # Client Connect: Takes user's name and chat rooms and sends connect message to server
                    user_name, *subscribed_rooms = data.strip().split()
                    send_data(tcp_sock, construct_connect_message(user_name, subscribed_rooms))
                # This block is responsible for handling direct messages or chat room messages once a user name is established.
                # Direct message: @Steve Hello Steve
                # Channel message: #networking Hello networking group!
                elif data.startswith(('@', '#')) and user_name: 
                    # Client Message: Takes messages from the user and sends them to the server.
                    # Messaging in chat rooms and direct messaging are handled here.
                    target, message = data.strip().split(' ', 1)
                    send_data(tcp_sock, construct_message(user_name, target, message))
                # This block handles disconnecting when the user types "quit" and Enter
                elif data.strip() == 'quit':
                    # Client Disconnect: Sends a disconnect message before shutting down
                    send_data(tcp_sock, construct_disconnect_message())
                    break
                
    # Handle client disconnect via CTRL+C
    except KeyboardInterrupt:
        send_data(tcp_sock, construct_disconnect_message())
        print("\nDisconnected via keyboard interrupt (CTRL+C)")

if __name__ == "__main__":
    main()
